## TharMediaLicense

*TharMediaLicense is a .dll file that gives you the ability to add a license in your programs.*

#### TharMediaLicense Features:
* Encrypt text
* Making a license file

#### TharMediaLicense Upcoming Features:
* Check a license file
* Add an expiration date
* Check for updates

#### How To Add TharMediaLicense In Your Programs
1. Add TharMediaLicense.dll To Your Program References
2. Add ``` using TharMediaLicense; ``` To Your Project
3. When you want to generate a license
```CSharp
string licensedoc = TharMediaLicense.TharMediaLicense.LicenseDoc(ProductName, ProductCustomer, ProductVersion);
System.IO.File.WriteAllText(@"C:\TharMediaLicense\TharMediaLicense.tml", licensedoc);
```
4.When You Want To Use Encryption
```CSharp
string licenseencrypt = TharMediaLicense.TharMediaLicense.LicenseHasher(ProductName, ProductVersion, ProductCustomer);
```
*Example TharMediaLicense.tml File*
```
Product Name= TharMediaTestApplication
Customer Name= Thymon Arens
Product Version= 1.0.0.0
Product Key= cT��w2�w|W� �jj9����o|?���=ЧO��e����?NT$��Y:�� �\�Y�����

#This license is made for TharMediaTestApplication
#Created on 09-03-16
#Software Version: ' 1.0.0.0 '
```
